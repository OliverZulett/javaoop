import models.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Period;


public class main {

    public static void main(String[] args) throws ParseException {

        Inventory inventory = new Inventory();
        inventory.printRentCase1();
        inventory.printRentCase2();
        inventory.printRentCase3();
        inventory.printRentCase4();
        
    }
    
}
