import models.*;

import java.text.ParseException;

import static models.Constants.*;

class Inventory {

    private MusicAlbum musicAlbum1;
    private MusicAlbum musicAlbum2;
    private MusicAlbum musicAlbum3;

    private Movie movie1;
    private Movie movie2;
    private Movie movie3;

    private VideoGame videoGame1;
    private VideoGame videoGame2;
    private VideoGame videoGame3;

    private Customer customer1;
    private Customer customer2;
    private Customer customer3;

    private MediaToRent mediaToRent1;
    private MediaToRent mediaToRent2;
    private MediaToRent mediaToRent3;
    private MediaToRent mediaToRent4;

    // BUILD INVENTORY
    public Inventory() throws ParseException {
        this.buildMovies();
        this.buildVideoGames();
        this.buildMusicAlbums();
        this.buildCustomers();
    }

    public void printRentCase1() throws ParseException {
        System.out.println( RENT_CASE_HEADER );
        this.mediaToRent1 = new MediaToRent( "MTR-001", dateFormatter.parse( "2020-05-10" ), dateFormatter.parse( "2020-05-14" ), new Media[]{this.movie1, this.movie2, this.videoGame2}, this.customer1 );
        this.mediaToRent1.printData();
        this.mediaToRent1.payRent( dateFormatter.parse( "2020-05-10" ) );
        this.mediaToRent1.printData();
        System.out.println( RENT_CASE_CUSTOMER_TITLE );
        System.out.println( RENT_CASE_CUSTOMER_DATA_TITLE );
        this.customer1.printData();
        System.out.println( RENT_CASE_FOOTER );
    }

    public void printRentCase2() throws ParseException {
        System.out.println( RENT_CASE_HEADER );
        this.mediaToRent2 = new MediaToRent( "MTR-002", dateFormatter.parse( "2020-05-20" ), dateFormatter.parse( "2020-05-26" ), new Media[]{this.movie2, this.videoGame3, this.musicAlbum1, this.videoGame2, this.musicAlbum3}, this.customer1 );
        this.mediaToRent2.printData();
        this.mediaToRent2.payRent( dateFormatter.parse( "2020-05-23" ) );
        this.mediaToRent2.printData();
        System.out.println( RENT_CASE_CUSTOMER_TITLE );
        System.out.println( RENT_CASE_CUSTOMER_DATA_TITLE );
        this.customer1.printData();
        System.out.println( RENT_CASE_FOOTER );
    }

    public void printRentCase3() throws ParseException {
        System.out.println( RENT_CASE_HEADER );
        this.mediaToRent3 = new MediaToRent( "MTR-003", dateFormatter.parse( "2020-06-13" ), dateFormatter.parse( "2020-06-19" ), new Media[]{this.movie2, this.videoGame2}, this.customer2 );
        this.mediaToRent3.printData();
        this.mediaToRent3.payRent( dateFormatter.parse( "2020-06-19" ) );
        this.mediaToRent3.printData();
        System.out.println( RENT_CASE_CUSTOMER_TITLE );
        System.out.println( RENT_CASE_CUSTOMER_DATA_TITLE );
        this.customer2.printData();
        System.out.println( RENT_CASE_FOOTER );
    }

    public void printRentCase4() throws ParseException {
        System.out.println( RENT_CASE_HEADER );
        this.mediaToRent4 = new MediaToRent( "MTR-004", dateFormatter.parse( "2020-06-13" ), dateFormatter.parse( "2020-06-19" ), new Media[]{this.movie2, this.videoGame2}, this.customer3 );
        this.mediaToRent4.printData();
        this.mediaToRent4.payRent( dateFormatter.parse( "2020-06-25" ) );
        this.mediaToRent4.printData();
        System.out.println( RENT_CASE_CUSTOMER_TITLE );
        System.out.println( RENT_CASE_CUSTOMER_DATA_TITLE );
        this.customer3.printData();
        System.out.println( RENT_CASE_FOOTER );
    }

    private void buildMovies() throws ParseException {
        // MOVIES
        this.movie1 = new Movie( "MO-001", "avatar", "A paraplegic Marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.", dateFormatter.parse( "2009-12-18" ), 1.2, 7.8, "James Cameron", new String[]{"Sam Worthington", "Zoe Saldana", "Sigourney Weaver"}, 2.42, new String[]{"Action"}, "USA" );
        this.movie2 = new Movie( "MO-002", "The Dark Knight", "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.", dateFormatter.parse( "2008-07-18" ), 1.4, 9.0, "Christopher Nolan", new String[]{"Christian Bale", "Heath Ledger", "Aaron Eckhart"}, 2.32, new String[]{"Action"}, "USA" );
        this.movie3 = new Movie( "MO-003", "Pulp Fiction", "The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.", dateFormatter.parse( "1994-09-14" ), 1.1, 8.9, "Quentin Tarantino", new String[]{"John Travolta", "Uma Thurman", "Samuel L. Jackson"}, 2.34, new String[]{"Crime", "Drama"}, "USA" );

        // Print Movies
        System.out.println( MOVIE_lABEL );
        System.out.println( MOVIE_DATA_TITLES );
        this.movie1.printMedia();
        this.movie2.printMedia();
        this.movie3.printMedia();
    }

    private void buildVideoGames() throws ParseException {
        //VIDEOGAMES
        this.videoGame1 = new VideoGame( "GA-001", "PES 20", "Experience unparalleled realism and authenticity in this year's definitive football game: PES 2020.", dateFormatter.parse( "2019-09-10" ), 1.9, 7.6, new String[]{"ps4", "xbox", "pc"}, "Konami Digital Entertainment", "Sport" );
        this.videoGame2 = new VideoGame( "GA-002", "Fallout 4", "Fallout 4 – their most ambitious game ever, and the next generation of open-world gaming.", dateFormatter.parse( "2015-11-10" ), 1.3, 8.0, new String[]{"Xbox One, PC, PS4"}, "Bethesda Game Studios", "Role-playing (RPG), Shooter" );
        this.videoGame3 = new VideoGame( "GA-003", "Dark Souls", "An action RPG and spiritual sequel to Demon's Souls (2009) in which the player embodies the Chosen Undead, who is tasked with fulfilling an ancient prophecy by ringing the Bells of Awakening in the dark fantasy setting of Lordran, an open world with intricate areas full of beasts, former humans gone hollow, and magical abominations whom the player must overcome in challenging and unforgiving combat.", dateFormatter.parse( "2011-08-22" ), 1.7, 8.6, new String[]{"Xbox One, PC, PS3"}, "FromSoftware", "Adventure, Hack and slash, (RPG)" );

        // Print Games
        System.out.println( GAME_LABEL );
        System.out.println( GAME_DATA_TITLES );
        this.videoGame1.printMedia();
        this.videoGame2.printMedia();
        this.videoGame3.printMedia();
    }

    private void buildMusicAlbums() throws ParseException {
        // MUSIC ALBUM
        this.musicAlbum1 = new MusicAlbum( "MA-001", "American Idiot", "Album de musica Rock", dateFormatter.parse( "2005-07-23" ), 0.8, "Green Day", 7, "rock" );
        this.musicAlbum2 = new MusicAlbum( "MA-002", "Nevermind", "Album de musica grunge", dateFormatter.parse( "1997-07-23" ), 0.7, "Nirvana", 9, "grunge" );
        this.musicAlbum3 = new MusicAlbum( "MA-003", "In Utero", "Album de musica grunge", dateFormatter.parse( "1999-07-23" ), 0.7, "Nirvana", 8, "grunge" );

        // Print Albums
        System.out.println( MUSIC_ALBUM_LABEL );
        System.out.println( MUSIC_ALBUM_DATA_TITLES );
        this.musicAlbum1.printMedia();
        this.musicAlbum2.printMedia();
        this.musicAlbum3.printMedia();
    }

    private void buildCustomers() {
        // CUSTOMERS
        this.customer1 = new Customer( "CU-001", "Javier", "Selada", "Calle de la esperanza #1001", 6543345 );
        this.customer2 = new Customer( "CU-002", "Hernan", "Gutierrez", "Villa soledad #33", 7654234 );
        this.customer3 = new Customer( "CU-003", "Rodrigo", "Aduriz", "Avenida de los olivos #54", 3453454 );

        //Print Customers
        System.out.println( CUSTOMER_LABEL );
        System.out.println( RENT_CASE_CUSTOMER_DATA_TITLE );
        this.customer1.printData();
        this.customer2.printData();
        this.customer3.printData();
    }
}
