package models;

import java.text.SimpleDateFormat;
import java.util.Date;

import static models.Constants.MUSIC_ALBUM_DATA_FORMAT;
import static models.Constants.dateFormatter;

public class MusicAlbum extends Media {
    private final String artist;
    private final Integer tracksNumber;
    private final String genre;

    public MusicAlbum(
            String id, String name, String description, Date releaseDate, Double costPerDay, String artist, Integer tracksNumber, String genre
    ) {
        super( id, name, description, releaseDate, costPerDay );
        this.artist = artist;
        this.tracksNumber = tracksNumber;
        this.genre = genre;
    }

    public void printMedia() {
        System.out.println( String.format( MUSIC_ALBUM_DATA_FORMAT, this.id, this.name, dateFormatter.format( this.releaseDate ), this.costPerDay, this.artist, this.tracksNumber, this.genre, this.description ) );
    }
}
