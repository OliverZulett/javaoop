package models;

import java.util.Arrays;
import java.util.Date;

import static models.Constants.*;

public class MediaToRent {

    private final String id;
    private final Date rentalDate;
    private final Date returningDate;
    private final Media[] mediaRented;
    private final Customer customer;

    private Boolean paymentStatus;
    private Double penaltyFee;
    private Double subTotal;
    private Double total;
    private Date payDate;
    private int pointsReward;

    public MediaToRent( String id, Date rentalDate, Date returningDate, Media[] mediaRented, Customer customer ) {
        this.id = id;
        this.rentalDate = rentalDate;
        this.returningDate = returningDate;
        this.mediaRented = mediaRented;
        this.customer = customer;
        this.paymentStatus = false;
        this.penaltyFee = 0.75;
        this.pointsReward = 0;
    }

    public void printData() {
        System.out.println( MEDIA_RENTED_TITLE );
        System.out.println( MEDIA_RENTED_DATA_TITLES );
        System.out.println( String.format( MEDIA_RENTED_DATA_TITLES_FORMAT, this.id, dateFormatter.format( this.rentalDate ), dateFormatter.format( this.returningDate ), doubleFormatter.format( this.penaltyFee ), this.paymentStatus, this.customer.getId(), Arrays.toString( this.getMediaList( "id" ) ) ) );
    }

    public void payRent( Date payDate ) {
        this.payDate = payDate;
        this.calculateSubTotal();
        this.calculatePenaltyFee();
        this.calculateTotal();
        this.calculateRewardPoints();
        this.printInvoice();
    }

    private void printInvoice() {
        System.out.println( PRINT_INVOICE_SEPARATOR_1 );
        System.out.println( PRINT_INVOICE_TITLE );
        System.out.println( PRINT_INVOICE_SEPARATOR_2 );
        System.out.println( "Surname: " + this.customer.getName() );
        System.out.println( "Rental date: " + dateFormatter.format( this.rentalDate ) );
        System.out.println( "Returning date: " + dateFormatter.format( this.returningDate ) );
        System.out.println( "Pay date: " + dateFormatter.format( this.payDate ) );
        System.out.println( PRINT_INVOICE_MEDIA_DETAIL_TITLE );
        System.out.println( Constants.PRINT_INVOICE_DATA_TITLES );
        for (Media mediaForPay : mediaRented) {
            System.out.println( String.format( "%-10s%-20s%-10s", mediaForPay.getId(), mediaForPay.getName(), mediaForPay.getCostPerDay() ) );
        }
        System.out.println( PRINT_INVOICE_SEPARATOR_2 );
        System.out.print( "Sub Total: " );
        System.out.printf( "%.2f", this.subTotal );
        System.out.print( "\nPenalty Fee: " );
        System.out.printf( "%.2f", this.penaltyFee );
        System.out.print( "\nTotal: " );
        System.out.printf( "%.2f", this.total );
        System.out.println( PRINT_INVOICE_SEPARATOR_1 );
        System.out.println( "Point reward: " + this.pointsReward );
        System.out.println( PRINT_INVOICE_SEPARATOR_2 );
    }

    private String[] getMediaList( String parameterType ) {
        String[] mediaIds = new String[this.mediaRented.length];
        for (int i = 0; i < this.mediaRented.length; i++) {
            switch (parameterType) {
                case "id":
                    mediaIds[i] = this.mediaRented[i].getId();
                    break;
                case "name":
                    mediaIds[i] = this.mediaRented[i].getName();
                    break;
                case "cost":
                    mediaIds[i] = String.valueOf( this.mediaRented[i].getCostPerDay() );
                    break;
            }
        }
        return mediaIds;
    }

    private void calculateSubTotal() {
        this.subTotal = 0.0;
        for (Media media : mediaRented) {
            this.subTotal += media.getCostPerDay();
        }
        Integer days = this.calculateDateDiff( this.rentalDate, this.payDate );
        if (days == 0) {
            this.subTotal *= this.calculateDateDiff( this.rentalDate, this.returningDate );
        } else {
            this.subTotal *= days;
        }
    }

    private void calculatePenaltyFee() {
        this.penaltyFee = 0.0;
        if (this.calculateDateDiff( this.returningDate, this.payDate ) > 0) {
            for (Media media : mediaRented) {
                if (media.getClass().getSimpleName().equals( "Movie" )) {
                    this.penaltyFee += media.getCostPerDay() * 0.75;
                }
            }
            this.penaltyFee *= this.calculateDateDiff( this.returningDate, this.payDate );
        }
    }

    private void calculateTotal() {
        this.total = this.subTotal + this.penaltyFee;
        this.paymentStatus = true;
    }

    private Integer calculateDateDiff( Date dateBefore, Date dateAfter ) {
        return (int) ((dateAfter.getTime() - dateBefore.getTime()) / (1000 * 60 * 60 * 24));
    }

    private void calculateRewardPoints() {
        String doubleForDivide = String.valueOf( this.subTotal );
        this.pointsReward = Integer.parseInt( doubleForDivide.substring( 0, doubleForDivide.indexOf( '.' ) ) );
        this.customer.setPoints( this.pointsReward );
    }
}
