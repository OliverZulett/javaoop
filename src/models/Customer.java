package models;

import static models.Constants.CUSTOMER_DATA_FORMAT;

public class Customer {
    private final String id;
    private final String name;
    private final String surname;
    private final String adress;
    private final Integer phone;
    private Integer points;


    public Customer(
            String id, String name, String surname, String adress, Integer phone
    ) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.adress = adress;
        this.phone = phone;
        this.points = 0;
    }

    public void printData() {
        System.out.println( String.format( CUSTOMER_DATA_FORMAT, this.id, this.name, this.surname, this.phone, this.points, this.adress ) );
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return this.id;
    }

    public void setPoints( Integer points ) {
        this.points += points;
    }
}
