package models;

import java.util.Arrays;
import java.util.Date;

import static models.Constants.VIDEO_GAME_DATA_FORMAT;
import static models.Constants.dateFormatter;

public class VideoGame extends Media {
    private final Double rating;
    private final String[] platforms;
    private final String developer;
    private final String type;
    private final String DATA_FORMAT = "%-10s%-20s%-15s%-20s%-10s%-30s%-30s%-40s%-10s";

    public VideoGame(
            String id, String name, String description, Date releaseDate, Double costPerDay, Double rating, String[] platforms, String developer, String type
    ) {
        super( id, name, description, releaseDate, costPerDay );
        this.rating = rating;
        this.platforms = platforms;
        this.developer = developer;
        this.type = type;
    }

    public void printMedia() {
        System.out.println( String.format( VIDEO_GAME_DATA_FORMAT, this.id, this.name, dateFormatter.format( this.releaseDate ), this.costPerDay, this.rating, Arrays.toString( this.platforms ), this.developer, this.type, this.description ) );
    }
}
