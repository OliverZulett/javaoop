package models;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class Constants {

    // Date Formatter
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final SimpleDateFormat dateFormatter = new SimpleDateFormat( DATE_FORMAT );

    // Double Formatter
    public static DecimalFormat doubleFormatter = new DecimalFormat( "0.00" );

    // Constants for Print data from media rented
    public static final String MEDIA_RENTED_TITLE = "\n=============MEDIA RENTED=============";
    public static final String MEDIA_RENTED_DATA_TITLES = String.format( "%-10s%-15s%-20s%-15s%-20s%-15s%-10s", "id", "rental date", "returning date", "penalty fee", "payment status", "customer", "media" );
    public static final String MEDIA_RENTED_DATA_TITLES_FORMAT = "%-10s%-15s%-20s%-15s%-20s%-15s%-10s";

    // Constants for Print Invoice
    public static final String PRINT_INVOICE_SEPARATOR_1 = "\n--------------------------------";
    public static final String PRINT_INVOICE_SEPARATOR_2 = "--------------------------------";
    public static final String PRINT_INVOICE_TITLE = "            INVOICE             ";
    public static final String PRINT_INVOICE_MEDIA_DETAIL_TITLE = "\n----------MEDIA DETAIL----------";
    public static final String PRINT_INVOICE_DATA_TITLES = String.format( "%-10s%-20s%-10s", "id", "name", "c/u" );

    // Constants for Print Rent Case
    public static final String RENT_CASE_HEADER = "\n====================== Rent Case ======================";
    public static final String RENT_CASE_CUSTOMER_TITLE = "\n===============CUSTOMER===============";
    public static final String RENT_CASE_CUSTOMER_DATA_TITLE = String.format( "%-10s%-10s%-10s%-10s%-10s%-10s", "id", "name", "surname", "phone", "point", "adress" );
    public static final String RENT_CASE_FOOTER = "\n=======================================================";

    // Constants for print customer data
    public static final String CUSTOMER_DATA_FORMAT = "%-10s%-10s%-10s%-10s%-10s%-10s";
    public static final String CUSTOMER_LABEL = "\n=============CUSTOMERS=============";

    // Constants for print movie data
    public static final String MOVIE_DATA_FORMAT = "%-10s%-20s%-15s%-15s%-10s%-20s%-50s%-10s%-20s%-10s%-10s";
    public static final String MOVIE_lABEL = "\n=============MOVIES=============";
    public static final String MOVIE_DATA_TITLES = String.format( "%-10s%-20s%-15s%-15s%-10s%-20s%-50s%-10s%-20s%-10s%-10s", "id", "name", "release date", "cost per day", "rating", "director", "actors", "duration", "genre", "country", "description" );

    // Constants for print music album data
    public static final String MUSIC_ALBUM_DATA_FORMAT = "%-10s%-20s%-20s%-20s%-20s%-20s%-20s%-10s";
    public static final String MUSIC_ALBUM_LABEL = "\n=============ALBUM MUSIC=============";
    public static final String MUSIC_ALBUM_DATA_TITLES = String.format( "%-10s%-20s%-20s%-20s%-20s%-20s%-20s%-10s", "id", "name", "release date", "cost per day", "artist", "tracks number", "genre", "description" );

    // Constants for print videogames data
    public static final String VIDEO_GAME_DATA_FORMAT = "%-10s%-20s%-15s%-20s%-10s%-30s%-30s%-40s%-10s";
    public static String GAME_LABEL = "\n=============GAMES=============";
    public static String GAME_DATA_TITLES = String.format( "%-10s%-20s%-15s%-20s%-10s%-30s%-30s%-40s%-10s", "id", "name", "release date", "cost per day", "rating", "platform", "developer", "type", "description" );
}







