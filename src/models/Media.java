package models;

import java.util.Date;

public abstract class Media {
    protected String id;
    protected String name;
    protected String description;
    protected Date releaseDate;
    protected Double costPerDay;

    public Media( String id, String name, String description, Date releaseDate, Double costPerDay ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.releaseDate = releaseDate;
        this.costPerDay = costPerDay;
    }

    protected void printMedia() {
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Double getCostPerDay() {
        return this.costPerDay;
    }
}
