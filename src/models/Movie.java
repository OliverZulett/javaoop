package models;

import java.util.Arrays;
import java.util.Date;

import static models.Constants.MOVIE_DATA_FORMAT;
import static models.Constants.dateFormatter;

public class Movie extends Media {
    private final Double imdbRating;
    private final String director;
    private final String[] actors;
    private final Double duration;
    private final String[] genre;
    private final String country;

    public Movie(
            String id, String name, String description, Date releaseDate, Double costPerDay, Double imdbRating, String director, String[] actors, Double duration, String[] genre, String country
    ) {
        super( id, name, description, releaseDate, costPerDay );
        this.director = director;
        this.actors = actors;
        this.duration = duration;
        this.genre = genre;
        this.country = country;
        this.imdbRating = imdbRating;
        this.name = name;
        this.description = description;
        this.releaseDate = releaseDate;
        this.costPerDay = costPerDay;
    }

    public void printMedia() {
        System.out.println( String.format( MOVIE_DATA_FORMAT, this.id, this.name, dateFormatter.format( this.releaseDate ), this.costPerDay, this.imdbRating, this.director, Arrays.toString( this.actors ), this.duration, Arrays.toString( this.genre ), this.country, this.description ) );
    }
}
